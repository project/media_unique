<?php /**
 * @file
 * Contains \Drupal\media_unique\Controller\DefaultController.
 */

namespace Drupal\media_unique\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the media_unique module.
 */
class DefaultController extends ControllerBase {

  public function media_unique_page() {
    if (!user_access('access media unique')) {
      drupal_set_message(t('You do not have access to media unique, check your permissions, contact the administrator.', FALSE, 'error', TRUE));
      drupal_goto('<front>');
    }
    else {
      drupal_goto('media-unique/batch');
    }
  }

  public function media_unique_batch_page() {
    $sha1_status_message = '';
    if (!user_access('access media unique')) {
      drupal_set_message(t('You do not have access to media unique batch page, check your permissions, contact the administrator.', FALSE, 'error', TRUE));
      drupal_goto('<front>');
    }
    $args = [':type' => variable_get('media_unique_bundle_to_process', 'image')];
    $queryHashes = db_query('SELECT count(entity_id) FROM {media_unique} where entity_id in (select fid from file_managed where type = :type)', $args);
    $result_count = $queryHashes->fetchColumn(0);
    if ($result_count == 0) {
      $sha1_status_message = t('Media Unique has not yet processed sha1 for your files, recommend batch operation to do this.');
      //  media_unique_batch_generate_sha1(NULL,NULL,NULL,NULL);
    }
    else {
      $args = [':type' => 'image'];
      //$test = db_query('SELECT COUNT(DISTINCT fid) FROM {file_managed} where type = :type', $args)->fetchField();
      $sha1_status_message = t('Media Unique has already processed sha1 values for !count files.  See your options below or go to the configuration page:', [
        '!count' => $result_count
        ]) . l('/admin/config/media/media_unique', 'admin/config/media/media_unique');
    }

    $batch_form = drupal_get_form('media_unique_batch_sha1_form');
    $batch_form = drupal_render($batch_form);
    return theme('media_unique_batch', [
      'sha1_status_message' => $sha1_status_message,
      'batch_form' => $batch_form,
      // Variables go here, you can add more variables like json_data.
      // These variables are used in templates/media_unique_batch.tpl.php.
    ]);
  }

}
