media_unique is a module that helps you delete duplicate files.  Media unique uses a sha1 hash method, it reads files and generates the hash, looks for other duplicate files.
